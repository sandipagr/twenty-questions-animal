package sa.animal;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class AnimalGameViewer extends JFrame {
   
    protected JTextArea myOutput;
    protected IAnimalModel myModel;
    protected String myTitle;
    protected JTextField myMessage;
    protected ArrayList<JComponent> myClickables;

    protected static JFileChooser ourChooser = new JFileChooser(System
            .getProperties().getProperty("user.dir"));

    public AnimalGameViewer(String title) {
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        myClickables = new ArrayList<JComponent>();
        
        JPanel panel = (JPanel) getContentPane();
        panel.setLayout(new BorderLayout());
        setTitle(title);
        myTitle = title;

        panel.add(makeInput(), BorderLayout.NORTH);
        panel.add(makeOutput(), BorderLayout.CENTER);
        panel.add(makeMessage(), BorderLayout.SOUTH);
        makeMenus();
        setEnabled(false);
        pack();
        setVisible(true);
    }

    public void setModel(IAnimalModel model) {
        myModel = model;
        myModel.setView(this);
    }

    protected JPanel makeMessage() {
        JPanel p = new JPanel(new BorderLayout());
        myMessage = new JTextField(30);
        p.setBorder(BorderFactory.createTitledBorder("message"));
        p.add(myMessage, BorderLayout.CENTER);
        return p;
    }

    protected JPanel makeInput() {
        JPanel p = new JPanel(new BorderLayout());
        JButton yes = new JButton("YES");
        p.add(yes, BorderLayout.WEST);
        yes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                myModel.processYesNo(true);
            }
        });
        JButton no = new JButton("NO");
        p.add(no, BorderLayout.EAST);
        no.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                myModel.processYesNo(false);
            }
        });
        myClickables.add(yes);
        myClickables.add(no);
        return p;
    }

    protected JPanel makeOutput() {
        JPanel p = new JPanel(new BorderLayout());
        myOutput = new JTextArea(20,40);
        p.setBorder(BorderFactory.createTitledBorder("game info"));
        p.add(new JScrollPane(myOutput), BorderLayout.CENTER);
        return p;

    }

    protected JMenu makeFileMenu() {
        JMenu fileMenu = new JMenu("File");

        fileMenu.add(new AbstractAction("Open File") {
            public void actionPerformed(ActionEvent ev) {
                int retval = ourChooser.showOpenDialog(null);
                if (retval == JFileChooser.APPROVE_OPTION) {
                    File file = null;
                    try {
                        file = ourChooser.getSelectedFile();
                        myModel.initialize(new Scanner(file));
                    } catch (FileNotFoundException e) {
                        AnimalGameViewer.this.showError("could not open "
                                + file.getName());
                    }
                }
            }
        });
        

        fileMenu.add(new AbstractAction("Open URL") {
            public void actionPerformed(ActionEvent ev) {
                String urlName = JOptionPane.showInputDialog(null,"Enter URL", "http://");
                if (urlName != null) {
             
                        URL url;
                        try {
                            url = new URL(urlName);  
                            myModel.initialize(new Scanner(url.openStream()));
                        } catch (MalformedURLException e) {
                           showError("bad url "+urlName);
                        }
                        catch (IOException e) {
                            showError("trouble reading url "+urlName);
                        }
                }
            }
        });

        fileMenu.add(new AbstractAction("Save") {
            public void actionPerformed(ActionEvent ev) {
                doSave();
            }
        });

        fileMenu.add(new AbstractAction("New game") {
            public void actionPerformed(ActionEvent ev) {
                doNewGame();
            }
        });
        JMenuItem newGameItem = fileMenu.getItem(fileMenu.getMenuComponentCount()-1);
        myClickables.add(newGameItem);
        
        fileMenu.add(new AbstractAction("Quit") {
            public void actionPerformed(ActionEvent ev) {
                System.exit(0);
            }
        });
        return fileMenu;
    }

    public void setEnabled(boolean value){
        for(JComponent comp : myClickables){
            comp.setEnabled(value);
        }
    }
    
    protected void makeMenus() {
        JMenuBar bar = new JMenuBar();
        bar.add(makeFileMenu());
        setJMenuBar(bar);
    }

    protected void doNewGame() {
        myModel.newGame();
    }

    private void doSave() {
        int result = ourChooser.showSaveDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            File file = ourChooser.getSelectedFile();
            try {
                FileWriter fw = new FileWriter(file);
                myModel.write(fw);
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void update(Collection elements) {
        myOutput.setText("");
        for(Object o : elements){
            myOutput.append(o+"\n");
        }
    }

    public void showMessage(String s) {
        myMessage.setText(s);
    }

    
    public void showDialog(String s) {
        JOptionPane.showMessageDialog(this, s, "Game Update",
                JOptionPane.INFORMATION_MESSAGE);
    }
    public void showError(String s) {
        JOptionPane.showMessageDialog(this, s, "Game Update",
                JOptionPane.INFORMATION_MESSAGE);
    }

    public void getNewInfoLeaf(String s){
        String response = JOptionPane.showInputDialog(this,s,"New Information Needed",JOptionPane.QUESTION_MESSAGE);
        if (response != null){
            myModel.addNewQuestion(response);
        }
        else {
            doNewGame();
        }
    }
    public void getDifferentiator(String s){
        String response = JOptionPane.showInputDialog(this,s,"New Question Needed",JOptionPane.QUESTION_MESSAGE);
        if (response != null){
            myModel.addNewKnowledge(response);
        }
        else {
            doNewGame();
        }
    }
}
