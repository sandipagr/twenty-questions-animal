package sa.animal;

/**
 * Tree Node Object
 * @author Sandip Agrawal
 *
 */
public class TreeNode {

    public enum Direction {
        LEFT, RIGHT, ROOT
    };

    public TreeNode yesLink;
    public TreeNode noLink;
    public String info;

    public TreeNode(String value, TreeNode lptr, TreeNode rptr) {
        info = value;
        yesLink = lptr;
        noLink = rptr;
    }

}
