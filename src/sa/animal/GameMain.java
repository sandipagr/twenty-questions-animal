package sa.animal;

/**
 * Animal Application Main
 * 
 * @author Sandip Agrawal
 */
public class GameMain {
    public static void main(String[] args) {
        AnimalGameViewer sv = new AnimalGameViewer("Twenty-Questions");
        IAnimalModel gm = new AnimalGameModel();
        sv.setModel(gm);
    }
}
