 ##Twenty Question Animal Game
 
 This program was written as a part of CompSci-100e (Spring 2007) course assignment at Duke University.
 
 The program basically allows the user to play a game of twenty questions where the user to picks an animal and the game tries to guess the animal by asking related questions. 